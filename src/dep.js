/**
 * @author Bo Yang Tang [bo@boyangtang.ca]
 */
this.floor = this.floor || {};

(function () {
  floor.protoInit = function protoInit(proto, vars) {
    var varsLength = vars.length,
      i = 0;

    for ( ; i < varsLength; i++) {
      (function(field) {
        Object.defineProperty(proto, field, {
          get: function() {
            return this.properties[field];
          },
          set: function(x) {
            this.properties[field] = x;
            this.notify('update');
          }
        });
      })(vars[i]);
    }
  };
})();