/**
 * @author Bo Yang Tang [bo@boyangtang.ca]
 */
this.floor = this.floor || {};

(function () {
  // new Canvas(canvasID) creates a Canvas object that manipulates an
  // existing <canvas> element with id string canvasID.
  //
  // usage: HTML:
  //              <canvas id="box"></canvas>
  //
  //        JavaScript:
  //              var canvas = new Canvas('box');
  //
  floor.Canvas = function Canvas(canvasID) {
    var cxt;
    this.canvasElement = document.getElementById(canvasID);
    cxt = this.canvasElement.getContext('2d');
    this.container = this.canvasElement.parentNode;
    this.layers = [];
    this.resize = false;
    this.refresh = true;
    this.renderProcess = null;
    this.callbacks = {
      pre: null,
      post: null
    };
    this.statsProcess = null;
    this.stats = {
      visible: false,
      targetFPS: 60,
      fps: 0,
      frames: 0,
      frametimeSum: 0,
      updateTime: 0,
      lastUpdate: NaN,
      currentUpdate: 0,
      updateDelay: 0,
      layer: undefined,
      text: undefined,
      objects: {
        total: 0,
        visible: 0
      }
    };
    this.render = {
      cxt: cxt,
      offset: [0, 0],
      position: [0, 0],
      width: 0,
      height: 0,
      scale: 1,
      defaultRender: {
        cxt: cxt,
        offset: [0, 0],
        position: [0, 0],
        scale: 1
      }
    };

    this.canvasElement.style.margin = '0';
    this.canvasElement.style.padding = '0';
    this.canvasElement.style.display = 'block';
    this.canvasElement.style.outline = 'none';
    this.canvasElement.tabIndex = 1000;
    this.container.style.padding = '0';
    this.container.style.backgroundColor = '#000';
    if (this.container == document.body) {
      this.container.style.margin = '0';
      this.canvasElement.width = window.innerWidth;
      this.canvasElement.height = window.innerHeight;
    } else {
      this.canvasElement.width = this.container.offsetWidth;
      this.canvasElement.height = this.container.offsetHeight;
    }

    this.width = this.canvasElement.width;
    this.height = this.canvasElement.height;

    this.stats.layer = new floor.Layer(this);
    this.stats.layer.bind(this.render);
    this.stats.layer.id = 'Canvas Performance Statistics';
    this.stats.layer.visible = false;
    this.stats.layer.overlay = true;
    this.stats.text = this.stats.layer.add(new floor.Text(0, 12));
    this.stats.text.colour = 'white';
    this.stats.text.font = 'Consolas, Courier New, Courier, monospace';
  };

  floor.Canvas.prototype.handleEvent = function(event) {
    console.log(event.type + ' event');
    switch (event.type) {
      case 'resize':
        this.resize = true;
        break;
    }
  };

  floor.Canvas.prototype.renderFrame = function renderFrame() {    
    if (this.callbacks.pre) this.callbacks.pre();

    this.stats.objects.visible = 0; // reset visible count
    this.stats.currentUpdate = performance.now(); // timestamp for frame render start
    this.stats.updateDelay = Math.round ( ( this.stats.currentUpdate - this.stats.lastUpdate ) * 1000 ) / 1000; // time elapsed since last frame start
    this.stats.lastUpdate = this.stats.currentUpdate; // store this frame start timestamp for next frame stats

    // main frame render loop
    if (this.resize) this.fillWindow();
    this.draw();

    this.stats.updateTime = Math.round ( ( performance.now() - this.stats.currentUpdate ) * 1000 ) / 1000; // time elapsed for render loop
    
    // parse render stats to verbose string if visible
    if (this.stats.visible) {
      this.stats.text.string = 'objects: ' + (this.stats.objects.total - 1).toString() +
                               '\ndrawn: ' + this.stats.objects.visible.toString() +
                               '\ndraw time: ' + this.stats.updateTime.toString() +
                               '\nframe time: ' + this.stats.updateDelay.toString() +
                               '\ntarget fps: ' + this.fps.toString() +
                               '\nfps: ' + this.stats.fps.toString() + ( this.stats.fps % 1 ? '' : '.0' ) +
                             '\n\nzoom: ' + this.render.scale.toString() +
                               '\ncenter: ' + this.render.position.toString() +
                               '\noffset: ' + this.render.offset.toString();
      this.stats.layer.draw();
    }

    if (this.callbacks.post) this.callbacks.post();
    this.stats.frames ++;
    this.stats.frametimeSum += this.stats.updateDelay;
    if ( this.renderProcess ) requestAnimationFrame(this.renderFrame.bind(this));
  };

  // startRender(pre-render function, post-render function) starts the canvas
  // draw loop. If a pre-render function is passed, it will run before the
  // draw. If a post-render function is passed, it will run after the draw.
  floor.Canvas.prototype.startRender = function startRender(pre, post) {
    var canvas = this;
    this.callbacks.pre = pre;
    this.callbacks.post = post;
    this.renderProcess = true;

    requestAnimationFrame(this.renderFrame.bind(this));

    this.statsProcess = setInterval(function(){
      canvas.stats.fps = Math.round ( 1000 / canvas.stats.frametimeSum * canvas.stats.frames * 10 ) / 10;
      canvas.stats.frames = 0;
      canvas.stats.frametimeSum = 0;
    }, 100);
  };

  floor.Canvas.prototype.stopRender = function stopRender() {
    clearInterval(this.statsProcess);
    this.renderProcess = false;
    this.statsProcess = null;
  };

  floor.Canvas.prototype.showStats = function showStats() {
    this.stats.visible = true;
    this.stats.layer.visible = true;
  };

  floor.Canvas.prototype.background = function background(cssColour) {
    this.container.style.backgroundColor = cssColour;
  };

  floor.Canvas.prototype.newLayer = function newLayer() {
    var layer = new floor.Layer(this);
    layer.bind(this.render);
    this.layers.push(layer);
    layer.id = 'Layer ' + this.layers.length.toString();
    return layer;
  };

  floor.Canvas.prototype.draw = function draw() {
    var i = 0;
    if (this.refresh) {
      this.clear();
      for (; i < this.layers.length; i++) {
        this.layers[i].draw();
      }
      this.refresh = false;
      //console.log('canvas drawn');
    }
  };

  floor.Canvas.prototype.fillWindow = function fillWindow() {
    var height = 0,
      width = 0,
      i = 0;

    if (this.container == document.body) {
      width = window.innerWidth;
      height = window.innerHeight;
    } else {
      width = this.container.offsetWidth;
      height = this.container.offsetHeight;
    }
    if (this.width != width || this.height != height) {
      this.canvasElement.width = width;
      this.canvasElement.height = height;
      this.width = this.canvasElement.width;
      this.height = this.canvasElement.height;
      for (; i < this.layers.length; i++) {
        this.layers[i].resize();
      }
      this.refresh = true;
      console.log('canvas size ' + width.toString() + ' x ' + height.toString());
    }
    this.resize = false;
  };

  // autofill
  // true: Canvas will resize to fit its parent element after window resize event
  // false: Canvas will stay at its original size
  Object.defineProperty(floor.Canvas.prototype, 'autofill', {
    set: function(x) {
      if (x == true) {
        window.addEventListener('resize', this, false);
        console.log('canvas will fill ' + this.container.tagName);
      } else {
        window.removeEventListener('resize', this, false);
        console.log('canvas will stay fixed size');
      }
    }
  });

  Object.defineProperty(floor.Canvas.prototype, 'fps', {
    set: function(x) {
      this.stats.targetFrameTime = 1000/x;
      this.stats.targetFPS = x;
    },
    get: function() {
      return this.stats.targetFPS;
    }
  });

  Object.defineProperty(floor.Canvas.prototype, 'showStats', {
    set: function(x) {
      if (x) {
        this.stats.visible = true;
        this.stats.layer.visible = true;
      }
    }
  });

  Object.defineProperty(floor.Canvas.prototype, 'width', {
    set: function(n) {
      this.render.offset[0] = Math.floor(n * 5) / 10;
      this.render.width = n;
      this.calibrate();
    },
    get: function() {
      return this.render.width;
    }
  });

  Object.defineProperty(floor.Canvas.prototype, 'height', {
    set: function(n) {
      this.render.offset[1] = Math.floor(n * 5) / 10;
      this.render.height = n;
      this.calibrate();
    },
    get: function() {
      return this.render.height;
    }
  });

  Object.defineProperty(floor.Canvas.prototype, 'center', {
    set: function(x, y) {
      this.render.position[0] = x;
      this.render.position[1] = y;
      this.calibrate();
    },
    get: function() {
      return this.render.position;
    }
  });

  floor.Canvas.prototype.zoom = function zoom(n) {
    this.render.scale *= n;
    this.render.cxt.scale(n, n);
  };

  Object.defineProperty(floor.Canvas.prototype, 'scale', {
    set: function(n) {
      this.render.scale = n;
      this.calibrate();
    },
    get: function() {
      return this.render.scale;
    }
  });

  floor.Canvas.prototype.move = function move(x, y) {
    this.render.position[0] -= x;
    this.render.position[1] -= y;
    this.render.cxt.translate(-x,-y);
  };

  floor.Canvas.prototype.getElementById = function getElementById(idString) {
    var i = 0,
      end = this.layers.length,
      result;

    for (; i < end; i++) {
      if (this.layers[i].id == idString) return this.layers[i];
      result = this.layers[i].getElementById(idString);
      if (result) return result;
    }

    return null;
  };

  floor.Canvas.prototype.calibrate = function calibrate() {
    this.render.cxt.setTransform(this.render.scale, 0, 0, this.render.scale, this.render.offset[0], this.render.offset[1]);
    this.render.cxt.translate(this.render.position[0], this.render.position[1]);
  };

  floor.Canvas.prototype.clear = function clear(a, b, c, d) {
    a = (a || 0);
    b = (b || 0);
    c = (c || this.width);
    d = (d || this.height);
    // Store the current transformation matrix
    this.render.cxt.save();

    // Use the identity matrix while clearing the canvas
    this.render.cxt.setTransform(1, 0, 0, 1, 0, 0);
    this.render.cxt.clearRect(a, b, c, d);

    //console.log("canvas cleared");

    // Restore the transform
    this.render.cxt.restore();
  };

  floor.Canvas.prototype.notify = function notify(msg) {
    switch (msg) {
      case 'add':
        this.stats.objects.total ++;
        break;
      case 'draw':
        this.stats.objects.visible ++;
        break;
      case 'update':
        this.refresh = true;
        break;
      default:
        break;
    }
  };

  // Layer

  floor.Layer = function Layer(canvas) {
    this.parent = canvas;
    this.drawables = [];
    this.width = this.parent.width;
    this.height = this.parent.height;
    this.id = '';
    this.visible = true;
    this.overlay = false;
    this.stats = {
      'objects': 0
    };
  };

  floor.Layer.prototype.bind = function bind(drawer) {
    this.render = drawer;
  };

  floor.Layer.prototype.add = function add(layerObject) {
    layerObject.parent = this;
    this.drawables.push(layerObject);
    this.stats.objects ++;
    this.parent.notify('add');
    return layerObject;
  };

  floor.Layer.prototype.getElementById = function getElementById(idString) {
    var i = 0,
      end = this.drawables.length;

    for (; i < end; i++) {
      if (this.drawables[i].id == idString) return this.drawables[i];
    }

    return null;
  };

  floor.Layer.prototype.isVisible = function isVisible(bounds) {
    return !(bounds[0][0] > this.width || bounds[0][1] > this.height || bounds[1][0] < 0 || bounds[1][1] < 0);
  };

  /*Layer.prototype.update = function update() {
    var time = this.parent.seconds();
    for (var i = 0; i < this.drawables.length; i++) {
      this.drawables[i].tick(time);
    }
  };*/

  floor.Layer.prototype.draw = function draw() {
    var render = this.overlay ? this.render.defaultRender : this.render;
    if (this.overlay) {
      render.cxt.save();
      render.cxt.setTransform(1, 0, 0, 1, 0, 0);
    }
    if (this.visible) {
      for (var i = 0; i < this.drawables.length; i++) {
        this.drawables[i].draw(render);
      }
      //console.log(this.id + ' drawn');
    }
    if (this.overlay) {
      render.cxt.restore();
    }
  };

  floor.Layer.prototype.resize = function resize() {
    this.width = this.parent.width;
    this.height = this.parent.height;
  };

  floor.Layer.prototype.notify = function notify(msg) {
    this.parent.notify(msg);
  };

  // LayerObject abstract

  floor.LayerObject = function LayerObject() {
    this.properties = {};
  };

  floor.LayerObject.prototype.init = function init(args, vars, defaults) {
    var i = 0,
      argsLength = args.length,
      varsLength = vars.length;

    for ( ; i < argsLength; i++) {
      this[vars[i]] = args[i];
    }

    for ( ; i < varsLength; i++) {
      this[vars[i]] = defaults[i];
    }
  };

  Object.defineProperty(floor.LayerObject.prototype, 'parent', {
    set: function(x) { this.properties.parent = x },
    get: function() { return this.properties.parent }
  });

  floor.LayerObject.prototype.notify = function notify(msg) {
    if (this.properties.parent) {
      this.properties.parent.notify(msg);
    }
  };

  Object.defineProperty(floor.LayerObject.prototype, 'id', {
    set: function(x) { this.properties.id = x },
    get: function() { return this.properties.id }
  });

  floor.Text = function Text(x,y,string,size,font,colour,align,position) {
    var names = ['x','y','string','size','font','colour','align','position'],
      defaults = [0,0,'',12,'Arial','#000','left','absolute'];
    floor.LayerObject.call(this);
    this.init(arguments,names,defaults);
    //var animation = {};
  };

  floor.Text.prototype = Object.create(floor.LayerObject.prototype);
  floor.Text.prototype.constructor = floor.Text;
  floor.protoInit(floor.Text.prototype,['x','y','string','size','font','colour','align','position']);

  /*Text.prototype.addAnimation = function(property, func, duration) {
    animation[property] = new Animation(func, duration);
  };
  */


  /*Text.prototype.tick = function(t) {

  };*/

  floor.Text.prototype.draw = function draw(render) {
    var i = 0,
      stringArray = this.string.split('\n'),
      stringArrayLength = stringArray.length,
      size = this.size * render.scale,
      x = this.x * render.scale + render.offset[0] + render.position[0],
      y = this.y * render.scale + render.offset[1] + render.position[1],
      cxt = render.cxt,
      bounds,
      width,
      visible;

    cxt.font = Math.round(this.size).toString() + 'px ' + this.font;
    cxt.fillStyle = this.colour;
    cxt.textAlign = this.align;

    if (this.position == 'absolute') {
      for ( ; i < stringArrayLength; i++) {
        width = cxt.measureText(stringArray[i]).width;
        bounds = [[x - width * (this.align == 'left' ? 0 : 1) / (this.align == 'center' ? 2 : 1), y + size * (i - 1)],
                  [x + width * (this.align == 'right' ? 0 : 1) / (this.align == 'center' ? 2 : 1), y + size * i]];
        if (this.parent.isVisible(bounds)) {
          cxt.fillText(stringArray[i], this.x, this.y + this.size * i);
          visible = true;
        }
        //console.log(stringArray[i] + '  ' + bounds.toString() + '  ' + this.parent.isVisible(bounds));
      }
    }
    else if (this.position == 'relative') {
      for ( ; i < stringArrayLength; i++) {
        cxt.fillText(stringArray[i],
                     x + this.parent.width / 2,
                     y + this.parent.height / 2 + size * i - (stringArray.length - 1.5) * size / 2);
      }
      visible = true;
    }
    if (visible) this.notify('draw');
  };

  floor.Line = function Line(o, d, scale) {
    var names = ['origin','direction','magnitude','colour','width'],
      defaults = [[0,0],0,0,'#000',1];
    floor.LayerObject.call(this);
    this.init(arguments,names,defaults);
  };

  floor.Line.prototype = Object.create(floor.LayerObject.prototype);
  floor.Line.prototype.constructor = floor.Line;
  floor.protoInit(floor.Line.prototype,['origin','direction','magnitude','colour','width']);

  floor.Line.prototype.draw = function draw(render) {
    var cxt = render.cxt, x1, y1, x2, y2, width, bounds;
    if (this.magnitude) {
      this.point = [this.origin[0] + Math.cos(this.direction) * this.magnitude,
                    this.origin[1] + Math.sin(this.direction) * this.magnitude];
    }
    x1 = (this.origin[0] + render.position[0]) * render.scale + render.offset[0];
    y1 = (this.origin[1] + render.position[1]) * render.scale + render.offset[1];
    x2 = (this.point[0] + render.position[0]) * render.scale + render.offset[0];
    y2 = (this.point[1] + render.position[1]) * render.scale + render.offset[1];
    width = this.width * render.scale;
    bounds = [[Math.min(x1, x2) - width, Math.min(y1, y2) - width],[Math.max(x1, x2) + width, Math.max(y1, y2) + width]];
    if (this.parent.isVisible(bounds)) {
      cxt.strokeStyle = this.colour;
      cxt.beginPath();
      cxt.moveTo(this.origin[0], this.origin[1]);
      cxt.lineTo(this.point[0], this.point[1]);
      cxt.lineWidth = this.width;
      cxt.stroke();
      cxt.lineWidth = 1;
      this.notify('draw');
    }
  };

  /*function animation(func, duration) {
    var begin = nan;
    var action = func;
    var pre = 0;
    var lifetime = (duration || infinity);
    var dead = false;

    this.tick = function(t) {
      if (dead) {
        return nan;
      }
      if (!begin) {
        begin = t;
      }
      var life = t - begin;
      if (lifetime <= life) {
        dead = true;
        life = lifetime;
      }
      var cur = action(life) - pre;
      pre += cur;
      return cur;
    };
  }*/

  floor.Arc = function Arc(x, y, r, a, b, cc, colour, fill) {
    var names =  ['x','y','r','a','b','cc','colour','fill'],
      defaults = [0,0,1,0,Math.PI,false,'#000',false];
    floor.LayerObject.call(this);
    this.init(arguments, names, defaults);
  };

  floor.Arc.prototype = Object.create(floor.LayerObject.prototype);
  floor.Arc.prototype.constructor = floor.Arc;
  floor.protoInit(floor.Arc.prototype,['x','y','r','a','b','cc','colour','fill']);

  floor.Arc.prototype.draw = function draw(render) {
    var cxt = render.cxt,
      bounds = [[(this.x - this.r + render.position[0]) * render.scale + render.offset[0],
                 (this.y - this.r + render.position[1]) * render.scale + render.offset[1]],
                [(this.x + this.r + render.position[0]) * render.scale + render.offset[0],
                 (this.y + this.r + render.position[1]) * render.scale + render.offset[1]]];
    if (this.parent.isVisible(bounds)) {
      cxt.beginPath();
      cxt.arc(this.x, this.y, this.r, this.a, this.b, this.cc);
      if (this.fill) {
        cxt.fillStyle = this.colour;
        cxt.fill();
      } else {
        cxt.strokeStyle = this.colour;
        cxt.stroke();
      }
      this.notify('draw');
    }
  };

  floor.Circle = function Circle(x, y, r, colour, fill) {
    var names = ['x','y','r','colour', 'fill'],
      defaults = [0,0,1,'#000',true];
    floor.Arc.call(this);
    this.init(arguments,names,defaults);
    this.properties.a = 0;
    this.properties.b = 6.28318530718;
  };

  floor.Circle.prototype = Object.create(floor.Arc.prototype);
  floor.Circle.prototype.constructor = floor.Circle;

})();