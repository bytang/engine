/**
 * @author Bo Yang Tang [bo@boyangtang.ca]
 */
 
this.floor = this.floor || {};

(function() {
  /** 
   *
   * @constructor 
   */
  floor.Clock = function Clock() {
    this.timeValues = [0, 0, 0, 0, 0, 0, 0];
    this.last = NaN;
    this.baseTime = 0;
    this.speed = 1;
  };

  floor.Clock.prototype.timeStrings = ['milliseconds', 'seconds', 'minutes', 'hours', 'days', 'weeks', 'years'];
  floor.Clock.prototype.timeMods = [1000, 60, 60, 24, 7, 52];

  /**
   * @property {number}
   * @name Clock#time
   */
  Object.defineProperty(floor.Clock.prototype, 'time', {
    get: function() {
      if (isNaN(this.last)) {
        return this.baseTime;
      }
      return this.baseTime + (performance.now() - this.last) * this.scale;
    },
    set: function(ms) {
      this.baseTime = ms;
      this.last = NaN;
      console.log('clock time set to ' + this.time.toString());
    }
  });

  /**
   * @property {number}
   * @name Clock#scale
   */
  Object.defineProperty(floor.Clock.prototype, 'scale', {
    get: function() {
      return this.speed;
    },
    set: function(n) {
      if (this.speed == n) {
        console.log('clock scale already at ' + this.speed.toString());
      } else {
        this.baseTime = this.time;
        this.speed = n;
        if (!isNaN(this.last)) {
          this.last = performance.now();
        }
        console.log('clock scale set to ' + this.speed.toString());
      }
    }
  });

  /**
   * @property {number}
   * @name Clock#milliseconds
   */
  Object.defineProperty(floor.Clock.prototype, 'milliseconds', {
    get: function() { return Math.floor(this.time) },
    set: function(t) { this.time = t }
  });

  /**
   * @property {number}
   * @name Clock#seconds
   */
  Object.defineProperty(floor.Clock.prototype, 'seconds', {
    get: function() { return Math.floor(this.time / 1000) },
    set: function(t) { this.time = t * 1000 }
  });

  /**
   * @property {number}
   * @name Clock#minutes
   */
  Object.defineProperty(floor.Clock.prototype, 'minutes', {
    get: function() { return Math.floor(this.time / 60000) },
    set: function(t) { this.time = t * 60000 }
  });

  /**
   * @property {number}
   * @name Clock#hours
   */
  Object.defineProperty(floor.Clock.prototype, 'hours', {
    get: function() { return Math.floor(this.time / 3600000) },
    set: function(t) { this.time = t * 3600000 }
  });

  /**
   * @property {number}
   * @name Clock#days
   */
  Object.defineProperty(floor.Clock.prototype, 'days', {
    get: function() { return Math.floor(this.time / 86400000) },
    set: function(t) { this.time = t * 86400000 }
  });

  /**
   * @property {number}
   * @name Clock#weeks
   */
  Object.defineProperty(floor.Clock.prototype, 'weeks', {
    get: function() { return Math.floor(this.time / 604800000) },
    set: function(t) { this.time = t * 604800000 }
  });

  /**
   * @property {number}
   * @name Clock#years
   */
  Object.defineProperty(floor.Clock.prototype, 'years', {
    get: function() { return Math.floor(this.time / 31449600000) },
    set: function(t) { this.time = t * 31449600000 }
  });

  /**
   * @memberof Clock.prototype
   */
  floor.Clock.prototype.importSettings = function importSettings(settingsArray) {
    this.last = settingsArray[0];
    this.baseTime = settingsArray[1];
    this.speed = settingsArray[2];
  };

  /**
   * @memberof Clock.prototype
   */
  floor.Clock.prototype.exportSettings = function exportSettings() {
    return [this.last, this.baseTime, this.speed];
  };

  /**
   * @memberof Clock.prototype
   */
  floor.Clock.prototype.reset = function reset() {
    console.log('clock reset at ' + this.time.toString());
    this.last = NaN;
    this.baseTime = 0;
    this.speed = 1;
  };

  /**
   * @memberof Clock.prototype
   */
  floor.Clock.prototype.start = function start() {
    if (isNaN(this.last)) {
      console.log('clock started at ' + this.time.toString());
      this.last = performance.now();
    } else {
      console.log('clock already ticking');
    }
  };

  /**
   * @memberof Clock.prototype
   */
  floor.Clock.prototype.stop = function stop() {
    this.baseTime = this.time;
    this.last = NaN;
    console.log('clock stopped at ' + this.time.toString());
  };
})();