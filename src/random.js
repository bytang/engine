/**
 * @author Bo Yang Tang [bo@boyangtang.ca]
 */

this.floor = this.floor || {};

(function() {
  /**
   * A namespace for pseudorandom generators.
   * @namespace
   */
  floor.random = floor.random || {};

  /**
   * Returns a float in the range [min, max).
   * 
   * @param {number} min
   * @param {number} max
   * @returns {number}
   */
  floor.random.floatNum = function floatNum( min, max ) {
    if ( min == max ) {
      return min;
    }
    return Math.random() * ( max - min ) + min;
  };

  /**
   * Returns an int in the range [min, max).
   *
   * @param {number} min
   * @param {number} max
   * @returns {number}
   */
  floor.random.intNum = function intNum( min, max ) {
    if ( min == max ) {
      return min;
    }
    return Math.floor( floor.random.floatNum( min, max ));
  };

  /**
   * Returns an element from array.
   *
   * @param {Array} array
   * @returns {*}
   */
  floor.random.element = function element( array ) {
    return array[ floor.random.intNum( 0, array.length ) ];
  };

  /**
   * Returns a hex colour string.
   *
   * @param {string|Array} [colourA] - start value for colour range
   * @param {string|Array} [colourB] - end value for colour range
   * @returns {string}
   */
  floor.random.hexColour = function hexColour( colourA, colourB ) {
    var rgb;
    if ( colourA && colourB ) {
      if ( typeof colourA == 'string' ) colourA = floor.colour.hexToRGB( colourA );
      if ( typeof colourB == 'string' ) colourA = floor.colour.hexToRGB( colourB );
      rgb = floor.random.colour( colourA, colourB );
    } else {
      rgb = floor.random.colour();
    }
    return floor.colour.rgbToHex( rgb );
  };

  /**
   * Returns a RGB array [R, G, B] with integer values.
   *
   * @param {Array} [colourA] - start value for colour range
   * @param {Array} [colourB] - end value for colour range
   * @returns {Array}
   */
  floor.random.colour = function colour( colourA, colourB ) {
    var red, green, blue, dR, dG, dB, range, start, stop, point, temp;
    //console.log( colourA, colourB );
    if ( colourA === undefined || colourB === undefined ) {
      red = floor.random.intNum( 0, 256 );
      green = floor.random.intNum( 0, 256 );
      blue = floor.random.intNum( 0, 256 );
    } else {
      // normalizes values within the range [0, 255]
      colourA.forEach( function( value, index, array ) {
        array[ index ] = Math.min( Math.max( 0, value ), 255 );
      });

      colourB.forEach( function( value, index, array ) {
        array[ index ] = Math.min( Math.max( 0, value ), 255 );
      });

      dR = Math.abs( colourA[ 0 ] - colourB[ 0 ] );
      dG = Math.abs( colourA[ 1 ] - colourB[ 1 ] );
      dB = Math.abs( colourA[ 2 ] - colourB[ 2 ] );
      range = Math.max( dR, dG, dB );

      if ( range == dR ) {
        start = colourA[ 0 ];
        stop = colourB[ 0 ];
      } else if ( range == dG ) {
        start = colourA[ 1 ];
        stop = colourB[ 1 ];
      } else {
        start = colourA[ 2 ];
        stop = colourB[ 2 ];
      }

      if ( start > stop ) {
        temp = start;
        start = stop;
        stop = temp;
      }

      point = floor.random.intNum( start, stop + 1 );

      if ( range == dR ) {
        red = point;
        green = ( point - colourA[ 0 ] ) * ( colourB[ 1 ] - colourA[ 1 ] ) / ( colourB[ 0 ] - colourA[ 0 ] ) + colourA[ 1 ];
        blue = ( point - colourA[ 0 ] ) * ( colourB[ 2 ] - colourA[ 2 ] ) / ( colourB[ 0 ] - colourA[ 0 ] ) + colourA[ 2 ];
      } else if ( range == dG ) {
        red = ( point - colourA[ 1 ] ) * ( colourB[ 0 ] - colourA[ 0 ] ) / ( colourB[ 1 ] - colourA[ 1 ] ) + colourA[ 0 ];
        green = point;
        blue = ( point - colourA[ 1 ] ) * ( colourB[ 2 ] - colourA[ 2 ] ) / ( colourB[ 1 ] - colourA[ 1 ] ) + colourA[ 2 ];
      } else {
        red = ( point - colourA[ 2 ] ) * ( colourB[ 0 ] - colourA[ 0 ] ) / ( colourB[ 2 ] - colourA[ 2 ] ) + colourA[ 0 ];
        green = ( point - colourA[ 2 ] ) * ( colourB[ 1 ] - colourA[ 1 ] ) / ( colourB[ 2 ] - colourA[ 2 ] ) + colourA[ 1 ];
        blue = point;
      }
      red = Math.round( red );
      green = Math.round( green );
      blue = Math.round( blue );
    }

    return [ red, green, blue ];
  };
})();