/**
 * @author Bo Yang Tang [bo@boyangtang.ca]
 */
this.floor = this.floor || {};

(function () {
  floor.Mouse = function Mouse(elementID) {
    var mouse = this;
    this.x = 0;
    this.y = 0;

    this.wheel = {
      up: null,
      down: null,
      left: null,
      right: null
    };
    this.mousedown = [];
    this.mouseup = [];

    this.checkout = [];
    this.checkin = [];

    this.buttonsToButton = [0, 2, 1]; 
    // buttons: 1 = left, 0 1 = right, 0 0 1 = middle (LTR binary)
    // button: 0 = left, 1 = middle, 2 = right (decimal)
    // buttonsToButton [0] = 0, [1] = 2, [2] = 1
    // e.g. for the buttons array [0, 0, 1] we can see that [0] and [1] are
    //      not down. This means that the 0 button (left) and 2 button (right) are up.
    //      The only button down is [2] = 1 (middle).    

    if (elementID.toLowerCase() == 'body') {
      this.capture = document.body;
    } else {
      this.capture = document.getElementById(elementID);
    }

    if (this.capture === null) {
      console.log('Mouse: cannot find element \'' + elementID + '\'');
    } else {
      this.capture.addEventListener('wheel', function(e) {
        //console.log(e.deltaX + ' ' + e.deltaY + ' ' + e.deltaZ);
        if (e.deltaX < 0) {
          mouse.doAction('wheel', 'left', e);
        } else if (e.deltaX > 0) {
          mouse.doAction('wheel', 'right', e);
        }
        if (e.deltaY < 0) {
          mouse.doAction('wheel', 'up', e);
        } else if (e.deltaY > 0) {
          mouse.doAction('wheel', 'down', e);
        }
      });

      this.capture.addEventListener('mousedown', function(e) {
        //console.log(e.button);
        mouse.x = e.clientX;
        mouse.y = e.clientY;
        mouse.doAction('mousedown', e.button, e);
      });

      this.capture.addEventListener('mouseup', function(e) {
        mouse.x = e.clientX;
        mouse.y = e.clientY;
        mouse.doAction('mouseup', e.button, e);
      });

      this.capture.addEventListener('mousemove', function(e) {
        mouse.x = e.clientX;
        mouse.y = e.clientY;
      });

      this.capture.addEventListener('mouseout', function(e) {
        //console.log(e.buttons);
        mouse.x = e.clientX;
        mouse.y = e.clientY;
        mouse.checkout = e.buttons.toString(2).split('').reverse();        
      });

      this.capture.addEventListener('mouseover', function(e) {
        mouse.x = e.clientX;
        mouse.y = e.clientY;
        mouse.checkin = e.buttons.toString(2).split('').reverse();
        mouse.checkin.forEach(function (value, index) {
          if (mouse.checkout[index] == '1') {
            if (mouse.checkin[index] == '0') {
              mouse.doAction('mouseup', mouse.buttonsToButton[index], e);
            }
          } else {
            if (mouse.checkin[index] == '1') {
              mouse.doAction('mousedown', mouse.buttonsToButton[index], e);
            }
          }
        });
      });
    }
  };

  floor.Mouse.prototype.setAction = function setAction(event, button, callback) {
    this[event][button] = callback;
  };

  floor.Mouse.prototype.doAction = function doAction(event, button, eventObject) {
    if (this[event][button]) this[event][button](eventObject);
  };

  floor.Mouse.prototype.onDown = function onDown(button, callback) {
    this.setAction('mousedown', button, callback);
  };

  floor.Mouse.prototype.onUp = function onUp(button, callback) {
    this.setAction('mouseup', button, callback);
  };

  floor.Mouse.prototype.onWheelScroll = function onWheelScroll(directionString, callback) {
    this.setAction('wheel', directionString.toLowerCase(), callback);
  };
})();